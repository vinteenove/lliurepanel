<?php

namespace Lliure\LliurePanel\Strategies;

use Laminas\Diactoros\Response;
use League\Route\Route;
use Lliure\LliurePanel\Response\ExceptionResponse;
use Lliure\LliurePanel\Tools\Environment;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Throwable;

class TwigStrategy extends \League\Route\Strategy\ApplicationStrategy
{

	public function invokeRouteCallable(Route $route, ServerRequestInterface $request): ResponseInterface{
		$controller = $route->getCallable($this->getContainer());

		$response = $controller($request, $route->getVars());

		$response = self::ResponseBuilder($response);

		return $this->decorateResponse($response);
	}

	public function getThrowableHandler(): MiddlewareInterface
	{
		return new class implements MiddlewareInterface
		{
			public function process(
				ServerRequestInterface $request,
				RequestHandlerInterface $handler
			): ResponseInterface {
				try {

					$response = $handler->handle($request);

					$twig = Environment::find($response);

					return ($twig === false)? $response: (new Response\HtmlResponse($twig->html()));

				} catch (Throwable $e) {
					return (new ExceptionResponse($e));
				}
			}
		};
	}

	protected static function ResponseBuilder(mixed $response): ResponseInterface{
		return ($response instanceof ResponseInterface)? $response: (is_string($response)? (new Response\HtmlResponse($response)): (new Response\JsonResponse($response)));
	}

}