<?php

namespace Lliure\LliurePanel\Exception;

use Throwable;

class DataException extends \Exception
{

	public function __construct(
		protected array|object $data,
		string $message = "",
		int $code = 0,
		?Throwable $previous = null
	){
		parent::__construct($message, $code, $previous);
	}

	/**
	 * @return array|object
	 */
	public function getData(): array|object
	{
		return $this->data;
	}
	
}